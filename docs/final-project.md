# Final Project

[Fiche A4 explicative](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Fichiers/A4%20explicative.pdf?inline=false)

# Synthèse

Mon projet final consiste en la création d'une famille d'objets du quotidien. Tous ces objets sont modélisés suivant la même technique que celle utilisée pour la modélisation de la Dondolo lors de son analyse. Cette technique est l'utilisation de l'outil **Balayage** sur fusion 360 et avec lequel une section définie (en bleu) suit un chemin donné (en vert) comme on peut voir ci-dessous.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Balayage-min.png)

C'est donc en me servant uniquement de cet outil de modélisation que j'ai réalisé mon ensemble d'objets.

## Le Lecteur

Le Lecteur est un porte-livre pensé pour la lecture avec une forme et une inclinaison adaptées au livre.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/PLC%20photos-min.png)

Fichiers:

- Modèle 3D : [Le Lecteur.stl](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/9bddad75f4cd5ff58b43ba4165870f24272195e8/docs/images/Final%20Project/Fichiers/Le%20Lecteur.stl?inline=false)

- Modèle d'impression : [Le Lecteur.gcode](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Fichiers/Le%20Lecteur_0.2mm_PLA_MK3_1d3h40m.gcode?inline=false)

## Le Classe

Le Classe a une double fonction, il peut soit servir de calle pour tenir les livres dans une bibliothèque, soit servir de présentoir pour mettre en avant un livre.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/CL4-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/CL%20poss-min.png)

Fichiers:

- Modèle 3D : [Le Classe.stl](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Fichiers/Le%20Classe.stl?inline=false)

- Modèle d'impression : [Le Classe.gcode](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Fichiers/Le%20Classe_0.2mm_PLA_MK3_14h2m.gcode?inline=false)

## Le Fluide

Le Fluide est un porte-bouteille dont la forme s'adapte à la courbure d'une bouteille et qui permet également un balancement afin de servir sans déplacer la bouteille.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/PB4-min.png)

Fichiers:

- Modèle 3D : [Le Fluide.stl](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Fichiers/Le%20Fluide.stl?inline=false)

- Modèle d'impression : [Le Fluide.gcode](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Fichiers/Le%20Fluide_0.2mm_PLA_MK3_1d9h13m.gcode?inline=false)

## L'Ardent

L'Ardent est un bougeoir adapté à la forme des bougies et qui se veut réversible afin de pouvoir y placer soit plusieurs petites bougies soit une grande.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/B%203-min.png )

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/B%20poss-min.png)

Fichiers:

- Modèle 3D : [L'Ardent.stl](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Fichiers/L'Ardent.stl?inline=false)

- Modèle d'impression : [L'Ardent.gcode](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Fichiers/L'Ardent_0.2mm_PLA_MK3_11h45m.gcode?inline=false)


# Recherche

Tout d'abord replacons ce travail dans son contexte. Lors du premier cours nous avons visité plusieurs expositions au [ADAM - Brussels Design Museum](https://designmuseum.brussels/). Lors de cette visite nous avons du choisir un objet sur lequel nous allions travailler pendant ce quadrimestre, au départ pour nous familiariser avec les machines et programmes utilisés au fablab, comme on peut le voir dans les [modules 1](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/maxime.wauthij/modules/module01/), [2](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/maxime.wauthij/modules/module02/), [3](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/maxime.wauthij/modules/module03/), [4](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/maxime.wauthij/modules/module03/) et [5](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/maxime.wauthij/modules/module05/).

Après cette période disons de formation nous avons commencé notre projet final qui consiste en un travail de réinterprétation de l'objet choisi.

## Objet de référence

L'objet que j'ai choisi à l'exposition est donc la chaise à bascule, le Dondolo, qui signifie chaise berçante en italien et qui a été conçu en 1968 par le duo italien Cesare Leonardi et Franca Stagi.

Ce que j'aime dans cette oeuvre c'est sa simplicité de forme et son coté très épuré à l'échelle globale de l'objet mais une grande présence de détails comme par exemple les rainures internes qui optimisent le rapport résistance / poids, ou encore les petites bordures présentes tout le long de la courbe.

Cette pureté de la forme est typique des créations des années 1960 car c'est l'époque des premiers voyages dans l'espace ce qui se traduit par apparence futuriste donnée aux éléments du quotidien.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Index/chaise_vue_de_profil-min.jpg)

## Les bases du projet

Pour introduire ce travail, nous avons du choisir parmis plusieurs mots celui qui selon nous, qualifierait notre approche artistique de la réinterprétation de l'objet. Ces mots sont :

**Référence** / **Influence** / **Hommage** / **Inspiration** / **Extension**.


Parmis ces 5 mots, j'ai donc choisi de travailler avec **Extension**. En effet, j'aime bien travailler sur un élément et le comprendre au maximum pour ensuite venir lui ajouter une petite touche personnelle.

En faisant des recherches sur la définition de ce mot je suis tombé le plus souvent sur des sens du mot où il était question d'aggrandissement de superficie. Cette définition me convient d'une certaine façon, mais je ne la trouve pas assez précise.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/def_extension_1-min.png)

J'ai donc continué à explorer et j'ai trouvé le sens que je cherchais dans les définitions de ce mot liées au monde de l'informatique.

Par contre, en informatique, il existe deux sens différents à ce mot. Le premier est le suffixe présent après le point dans le nom d'un fichier et qui défini son type. Mais ce n'est pas ce sens ci qui m'intéresse.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/extension_info_point-min.png)

Le second sens du mot en informatique considère les extensions que l'on peu apporter à un programme. Dans ce cas ci, il est donc clairement question d'un ajout de contenu ou de nouvelles fonctionnalités et c'est exactement ce que je recherche.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/def_extension_ok-min.png)

## Application

Voici donc ce que donne ce concept appliqué à mon projet. On observe en vert le Dondolo qui est l'objet même sur lequel j'ai travaillé et en rouge l'extension que je viens lui apporter.

Ce filet rouge est un rangement et vient donc ajouter une nouvelle fonctionnalité à la chaise.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%203/chaise_verte_c%C3%B4t%C3%A9-min.jpg)

Pour continuer sur le même principe, je vais tenter d'ajouter des fonctionnalités au Dondolo, tout en lui apportant un minimum de modification.

J'ai donc d'abord pensé à une autre façon d'ajouter des rangements en plaçant des petits quasiers à l'arrière de l'assise mais comme on peut voir sur l'image ci-dessous il y a un trop grand ajout de matière et c'est une chose que j'aimerais éviter.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/rangement%202-min.png)

J'ai ensuite penser à rendre la courbe du Dondolo modulable afin de pouvoir changer la position d'assise mais je n'ai pas trouver de solution pour rendre cela possible sans ajouter d'acroches ou d'élément extérieur.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/croquis%20modularit%C3%A9-min.png)

Pour rester dans ce but de changer la façon de s'installer dans la chaise j'ai imaginé une autre façon de faire en m'inspirant cette fois de la toute première modification que j'avais apporté à l'objet mais cette fois ci à une autre échelle.

En modifiant la courbure de la chaise et par un système de crochet intégrés à la courbe, on peut maintenant transformer cette chaise à bascule en tranzat.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/chaise%20transat-min.jpg)

Mais cette modification ne respecte toujours pas la définition du mot extension sur laquelle j'ai décidé de travailler car je modifie l'objet existant.

Mon intervention doit donc apporter une nouvelle fonction à la Dondolo mais à la façon d'un programme qu'on viendrait ajouter sur un ordinateur, ce plug-in doit pouvoir laisser la chaise intact une fois retiré.

Une idée m'est donc venue en installant le tranzat sur le test précédent. Trop tendu, il m'a fait penser à un élément que l'on viendrait fixer à la chaise et qui créerait une couverture, une protection. Ce nouvel ajout laisse bien l'objet d'origine intact et permet une utilisation extérieure de la chaise en protegeant l'occupant aussi bien du soleil que de la pluie.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/couverture%20de%20face-min.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/couverture%20de%20dos-min.jpg)

## Passage à une taille réelle.

La prochaine étape dans mon travail est de passer à l'échelle 1:1 et donc la création d'un objet à taille réelle.

Le plug-in dont on peut observer l'évolution plus haut n'est malheureusement pas réalisable à taille réelle car cela nécessiterait une utilisation de la Shaper ou de la CNC et ma chaise deviendrait un ensemble de tranche de bois collées ensembles ce qui n'est pas en accord avec la façon dont j'ai modélisé l'objet.

J'ai donc décidé de changer de mot de base et de choisir le mot **Inspiration** afin de pouvoir créer de plus petits objets à taille réelle.

Pour définir ce mot j'ai décidé de me baser sur le Larousse.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/def%20inspiration-min.png)

On observe dans cette définition l'utilisation du mot **Influence** qui était présent dans la liste de base j'ai donc tenu à le définir lui aussi, toujours en ma basant sur le Larousse.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/def%20influence-min.png)

L'action décrite ici et que le Dondolo va exercer sur mes nouveaux objets, est le fait que je vais réutiliser un principe de base de sa modélisation comme élément de départ pour la création de mes modèles.

Ce principe est, comme décrit dans le [module 2](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/maxime.wauthij/modules/module02/), le fait que l'objet est générer par un profil qui suit un chemin, à l'aide de l'outil balayage de fusion360, et cette combinaison génère une forme qui est tout de suite la forme finale.

Mon objectif va être de créer une série d'objet dont la forme sera basée sur ce principe.

Leur taille sera limité par les capacités de l'imprimante Prusa à disposition, dont l'utilisation est expliquée dans le [module 3](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/maxime.wauthij/modules/module03/), car je veux que la création respecte la modélistion et le principe que l'objet possède une forme unique et ne peut donc pas être la combinaison de deux pièces crées séparemment.

## Les MINI-porteurs

Sur chacune des images d'esquisse ci-dessous, le trait vert représente le chemin de balayge que la section bleue présente à coté va suivre.

On retrouve également pour chacun des objets une première version inspirée directement de la technique de modélisation de la Dondolo, suivie des versions retravaillées.

### 1 : Le Lecteur

#### Le rôle du Lecteur est de vous tenir votre livre pour vous faciliter la tâche.

#### Modèle n°1

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/support%20livre%20plat%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/support%20livre%20plat-min.png)

Pour cette seconde version, j'ai cherché à ammener une certaine courbure dans le dossier du livre tout en gardant la zone de support plane.

#### Modèle n°2

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/support%20livre%20courbe%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/support%20livre%20courbe-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/PLC%20photos-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Lecteur%20sch%C3%A9ma-min.png)

### 2 : Le Classe

#### Le Classe vous propose un endroit sûr où entreposer vos prochaines lectures.

#### Modèle n°1

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/rangement%20livre%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/rangement%20livre-min.png)

Dans cette seconde version on retrouve à nouveau un travail sur les courbes mais cette fois ci, dans le but d'apporter une certaine légèreté à l'objet.

#### Modèle n°2

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/range%20livre%20courbe%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/range%20livre%20courbe%20-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/RLC%20photos-min.jpg)

J'ai ensuite remis en question la fonctionnalité de l'objet. En effet, Sa fonction première étant le rangement de livre, il n'est pas efficace car il ne peut contenir que 4 livres maximum qui ne peuvent y être placés que dans une seule position en raison des ses petites dimensions.

C'est alors que j'ai décidé de repenser mon objet plus comme un système de calle qu'un espace de rangement.

#### Modèle n°3

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/calle%20livre%201%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/calle%20livre%201-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/CL1-min.png)

Ce premier modèle s'inspire des calles livres que l'on peut trouver sur le marché. Son gros défaut ici, est sa légèreté. En effet ses semblables industriels sont conçus en métal et sont donc suffisament lourds que pour maintenir les livres. Celui ci glisse.

Pour résoudre ce problème j'ai décidé d'ajouter à mon objet un emplacement prévu pour un livre afin que ce dernier joue le rôle de contrepoids et maintienne l'objet.

#### Modèle n°4

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/calle%20livre%202%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/calle%20livre%202-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/CL2-min.png)

Ces deux modèles, en particulier celui ci-dessus, m'ont donné l'envie d'ajouter la fonction de présentoire à mon objet. Il a donc fallut que j'adapte le forme pour que mon objet puisse jouer le rôle à la fois de présentoire et de calle pour livre en utilisant le poids du livre qu'il expose comme contre-poids.

#### Modèle n°5

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/pr%C3%A9sentoire%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/presentoire-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/CL3-min.png)

On peut observer sur les photos ci-dessous deux manières différentes d'employer cet objet.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Pr%C3%A9sentoir%20possibilit%C3%A9s-min-min.png)

A ce stade, la forme générale de l'objet est convaincante mais il y a toujours un élément très dérangeant qui est la partie inférieure trop massive lors de la deuxième utilisation comme on peut le voir sur la photo de droite ci-dessus.

J'ai également retiré la paroie servant à éviter que les livres ne glissent et je l'ai remplacée par un jeux de courbes dans la forme de l'objet, qui vient remplir le même rôle.

#### Modèle n°6

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/presentoire%20v2%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/presentoir%20v2-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/CL4-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/CL%20poss-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Classe%20sch%C3%A9ma-min.png)

### 3 : Le Fluide

#### Le Fluide maintiens vos bouteilles en toute stabilité.

#### Modèle n°1

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/support%20bouteille%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/support%20bouteille-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/PB%20photos-min.jpg)

Dans ce cas-ci, le travail de la courbure cherche à offrir une stabilité à l'objet pour éviter un effet de balancement rencontré avec **le Lecteur** qui dans ce cas ci aurait posé problème.

Il a égalemment fallu rendre plat l'espace de support à la base de l'objet.

#### Modèle n°2

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/porte%20bouteille%20courbe%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/porte%20bouteille%20courbe-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/PBC%20photos-min.jpg)

La forme de ce second modèle est très convaincante mais il y a encore trop d'éléments, j'ai donc essayé de l'alléger tout en conservant les parties importantes pour sa fonction, comme le creux et le plateau destinés à maintenir la bouteille, ou encore la courbe en contact avec le sol qui permet un balancement de l'objet.

#### Modèle n°3

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/fluide%20v3%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/fluide%20v3-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/PB%202-min.png)

Avec ce troisième modèle, j'ai enfin obtenu la forme que je cherchais, un parfait mélange entre légèreté et fonctionnalité.

Il reste malgrés tout quelques petits éléments à mettre au point comme adapter l'espace de support au gabarit d'une bouteille afin d'améliorer son maintien au maximum. J'ai également remarqué que mon objet était trop souple et se pliait sous le poids d'une bouteille pleine, j'ai alors modifié certains paramètres d'impression comme le remplissage afin de résoudre ce problème de flexion.

#### Modèle n°4

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/fluide%20v4%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/fluide%20v4%20-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/PB%203-min.png)

Afin de perfectionner **Le Fluide**, j'ai légèrement agrandi l'espace de support pour que la bouteille s'y place parfaitement et j'ai également amélioré le remplissage pour rigidifier l'objet.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/PB4-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Fluide%20sch%C3%A9ma-min.png)


### 4 : L'Ardent

#### L'Ardent se propose de tenir les bougies qui vous éclaireront.

Cette catégorie représentent des tests dans lesquels j'ai tenté de voir ce que je pouvais obtenir avec de long chemin et des petites sections avec pour objectif de créer un bougeoire.

Le premier présente plusieurs espaces de pose au niveau du sol et un espace central surélevé.

#### Modèle n°1

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/bougeoir%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/bougeoir-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/B1-min.png)

L'ensemble étant beaucoup trop souple et fragile, l'espace de pose central n'était pas du tout maintenu.

Le second ne possède qu'un seul emplacement surélevé dans lequel la bougie se calle entre les courbes.

#### Modèle n°2

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/bougeoir%20sur%20pied%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/bougeoir%20sur%20pied-min.png)

Après avoir imprimé le premier modèle et m'être rendu compte de la fragilité de ces éléments très fin, j'ai décidé de revenir vers une forme plus basique et solide.

#### Modèle n°3

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/ardent%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/ardent-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/B2-min.png)

J'ai ensuite décidé d'adapter ce modèle à une certaine forme de bougie. J'ai donc retravailler sa forme pour qu'elle épouse les courbes des bougies rondes choisies mais également pour qu'il soit réversible et qu'il puisse ainsi acceuillir deux tailles de bougies différentes.

#### Modèle n°4

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Bougeoir%20bulle%20esquisse-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Bougeoir%20bulle%20-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/B%203-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/B%20poss-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/Ardent%20sch%C3%A9ma-min.png)

### La couleur

L'objectif de mes **MINI-porteur**, est de créer une famille d'objet ayant tous en commun leur technique de modélisation. C'est donc assez logiquement que j'ai décidé de tous les réaliser dans la même couleur.

La question était donc maintenant : '' Quelle couleur?''

J'ai donc décidé de travailler avec le bleu pour différentes raisons:

- La première est que le bleu est la couleur qui rappelle l'eau et induit donc une idée de mouvement comme les courbes fort présentes dans mes modèles.

- La seconde raison est un peu en opposition au concept de mouvement. En effet la fonction même de mes objets est de supporter, ce qui demande de la stabilité et le bleu est considéré comme une couleur apaisante qui inspire la sérénité, ce qui renforce l'aspect stable de mes modèles.

Ensuite, j'ai du choisir avec quel bleu j'allais travailler car comme pour toutes les couleurs, il en existe énormément.

J'ai réalisé, au fil de mes tests et de l'évolution de mes modèles, qu'un bleu clair donnait la sensation que l'objet flotte dans l'espace et y est un peu perdu. Tandis qu'un bleu trop foncé apporte un côté imposant voir dérangeant au modèle.

J'ai donc décidé d'opter pour un bleu assez foncé qui permettrait d'offrir une bonne présence à mes **MINI-porteur** et ainsi renforcer leur fonction de support. Mais pas trop foncé non plus pour éviter qu'ils ne prennent trop de place et viennent géner l'espace. Le bleu en question est donc celui présent dans la gommette ci-dessous.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/couleur.png)

# Exposition

## Photos

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/photo%20globale-min.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/photos%20tables-min.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Final%20Project/photo%20globale%20+%20moi-min.jpg)

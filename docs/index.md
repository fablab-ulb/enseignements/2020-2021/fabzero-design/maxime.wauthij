![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Index/avatar-photo-min.jpg)

# A propos

Bonjour, je m'appelle Maxime Wauthij.
Je suis né à Uccle en 1997 et j'ai toujours vécu à Bruxelles.

Mes deux grandes occupations ont toujours étées le sport et les jeux vidéos mais je dois admettre que depuis mon entrée à l'université et surtout an archi, je consacre, malheureusement, beaucoup moins de temps au sport

### Mon parcours

J'ai commencé l'université avec deux années en polytechnique. Cela ne me convenant pas du tout je me suis redirigé vers l'architecture et je suis maintenant en première master avec encore malheureusement un cours de ma troisième année de bachelier.

Bien que j'ai, aujourd'hui, trouvé des études qui me plaisent et dans lesquelles je peux m'épanouir je n'ai quasiment aucune idée de ce que je veux faire plus tard. J'ai toujours eu beaucoup de mal à me projetter dans l'avenir donc m'imaginer où je serai après mes études est un exercice très compliqué pour moi.

# Objet choisi à l'exposition.

L'objet que j'ai choisi à l'exposition est donc la chaise à bascule, le Dondolo, qui signifie chaise berçante en italien et qui a été conçu en 1968 par le duo italien Cesare Leonardi et Franca Stagi.

Ce que j'aime dans cette oeuvre c'est sa simplicité de forme et son coté très épuré à l'échelle globale de l'objet mais une grande présence de détails comme par exemple les rainures internes qui optimisent le rapport résistance / poids, ou encore les petites bordures présentes tout le long de la courbe.

Cette pureté de la forme est typique des créations des années 1960 car c'est l'époque des premiers voyages dans l'espace ce qui se traduit par apparence futuriste donnée aux éléments du quotidien.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Index/chaise_vue_de_profil-min.jpg)

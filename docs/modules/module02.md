# 2. Mes débuts sur Fusion 360

Dans ce module je vais expliquer comment j'ai modèlisé mon objet en 3D sur fusion.

## Etapes de la modélisation.

### Etape 1: Tracer l'esquisse.

Tout d'abord, il fallait que je réfléchisse à la position dans laquelle j'allais modéliser ma pièce. En effet, à cause de sa forme courbe, il a fallu que je la modélise couchée pour rendre l'impression qui va suivre possible.

J'ai donc tracé le profil de mon objet avec l'outil "créer une esquisse".

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%202/esquisse_etape_1-min.png)

### Etape 2: Relever L'objet.

Pour cette étape, il m'a suffi de relever l'esquisse avec l'outil "extrusion".

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%202/forme_relev%C3%A9e_etape_2-min.png)

### Etape 3: arrondir l'objet.

J'ai ici utilisé l'outil "congé" afin d'enlever tous les coins de mon esquisse qui ne sont pas présent sur l'objet.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%202/forme_courb%C3%A9e_%C3%A9tape_3-min.png)

### Etape 4: Creuser la forme.

Il a fallu pour cette étape que j'ajoute les rainures présentes sur la face intérieure de la chaise. Mais je n'ai trouvé aucun moyen d'y parvenir en partant du modèle que j'avais, j'ai donc de recommencer ma forme depuis le début en prenant ces rainures directement en compte.

Pour cela, j'ai donc repris l'esquisse de la forme de la chaise que j'avais à la base que j'ai directement courbée avec l'outil "congé" à nouveau.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%202/esquisse_H_courbe_etape4-min.png)

J'ai ensuite, dans la même esquisse ajouté un tracé perpendiculaire au premier qui représente cette fois le profilé de la chaise et qui contient les rainures.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%202/esquisse_double_etape_4-min.png)

Pour finir, j'ai utilisé l'outil "balayage" pour composer les deux tracés présents dans mon esquisse.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%202/Balayage_etape_4-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%202/etape_4_fin-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%202/modele_3D-min.png)

# 1. Mes début sur GitLab.

## Mise en place de Git.

Pour commencer, j'ai du installer Bash shell sur mon ordinateur j'ai pour cela suivi les instructions données sur ce [site](https://korben.info/installer-shell-bash-linux-windows-10.html). Etant sur windows 10, il ne m'a fallu que quelques changements de réglages sur le pc et un redémarrage pour y arriver.

J'ai ensuite téléchargé Git et suivi les instructions que l'on trouve dans le fichier [Documentation.md](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git).

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%201/config_Git-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%201/configurastion_git-min.png)

## La clef SSH.

En me fiant au instructions sur ce [lien](https://docs.gitlab.com/ee/ssh/README.html), j'ai décidé de créer une clé SSH ED25519.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%201/cr%C3%A9ation_cl%C3%A9_SSh-min.png)

Une fois cette clé crée, on peut la retrouver dans les dossier de l'ordinateur. Dans ce dossier, on trouvera deux clé, une privée et une publique (.pub). Il sera toujours préférable de travailler avec la publique et de garder la privée secrète.

Il faut dés lors, ouvrir ce fichier .pub à l'aide d'un programme de traitement de texte comme Atom dans mon cas et copier la clé qui s'affiche à l'écran.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%201/cle_ssh_atom-min.png)

Ensuite, il faut retourner sur GitLab, dans les paramètre du compte, trouver l'onglet SSH Kheys et y copier la clé pour l'enregistrer.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%201/settings-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%201/ssh_keys-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%201/coll%C3%A9_ssh-min.png)

Afin de vérifier que tout est en ordre, taper la commande suivante dans le terminal et appuyer sur enter.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%201/verif_finale-min.png)

Il faut finalement lier les dossier de GitLab à des dossiers en local sur le le pc. pour cela, cloner la clé ssh et la copier dans un pogramme de texte comme Atom. Ensuite, il faut se rendre sur le dossier du pc que l'on souhaite mettre en lien et y ouvrir Bash.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%201/ouvrir_terminal-min.png)

Une fois Bash ouvert dans le bon fichier encoder la clé SSH et appuyer sur Enter.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%201/lien_dossier-min.png)

Maintenant que ce nouveau fichier est créé, il faut se rendre sur Atom et l'ouvrir afin de pouvoir le retravailler.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%201/ouverture%20dans%20Atom-min.png)

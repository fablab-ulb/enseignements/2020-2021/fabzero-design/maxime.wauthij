# 4. Découpeuse laser

Nous avons, cette semaine, apris à nous servir d'une découpeuse laser. pour nous familiariser avec cette machine, l'exercice était de concevoir une lampe qui pourrait être assemblée avec des éléments en 2D. Cette lampe devant être inspirée d'une manière ou d'une autre de l'objet que nous avons choisi à l'expo.

## Définition de la forme.

Dans mon cas, j'ai donc du m'inspirer d'une chaise à bascule. Selon moi, l'élément qui caractérise cette chaîse est sa capacité à se mettre en mouvement.

En me basant sur cette idée j'ai donc décidé de donner à ma lampe la forme de l'objet qui représente le mieux le concept de mouvement, la roue. Au départ d'une feuille, il suffi de découpé un rectangle et de le rouler pour obtenir une roue.

Il a ensuite fallu que je trouve un moyen de fermer cette roue et de faire en sorte qu'elle conserve sa forme ronde. J'ai décidé pour cela d'utiliser les barreaux de la roue qui, une fois courbés, peuvent s'emboiter les uns dans les autres. On observe donc ici sur les trois images qui suivent l'évolution de la forme des barreaux.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/roue_carr%C3%A9e-min.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/roue_triangle-min.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/test_profil-min.jpg)

## Emploi de la machine.

J'expliquerai ici, le processus qui a mené des maquettes en papier à la forme découpée au laser.

Tout d'abord il faut tracer la forme sur autocad et l'exporter en .pdf. La découpeuse laser ne lisant que des fichiers vectoriels, il faut transformer le .pdf en .svg, pour ma part, je suis passé par illustrator pour réaliser cette action.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/export_svg-min.png)

Une fois en possession du fichier .svg, placer le sur une clé usb pour pouvoir le mettre sur la machine. Une fois la clé dans la machine lancer le programme lié à la découpeuse laser. Une fois dedans, ouvrer le fichier cible.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/ouvrir_fichier-min.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/erreur-min.jpg)

Si le message d'erreur ci-dessus apparaît, il va falloir passer le fichier sur Inkscape pour traiter le document afin que le premier programme puisse le lire.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/ouvrir_inkskape-min.jpg)

Sur inkscape , il faudra par exemple dégrouper les traits des dessins (première image ci-dessous), changer la taille de l'objet en faisant varier les unités, ou encore retirer des traits inutiles en faisant varier l'épaisseur de ces traits (deuxième image ci-dessous).

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/ungroup-min.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/taille_ds_traits-min.jpg)

Une fois le fichier traité, on peut retourner sur le premier programme et ouvrir à nouveau le fichier cible. Cette fois-ci normalement, le dessin devrait apparaîttre sur l'espace de travail à l'écran.

Maintenant que l'on a le bon fichier ouvert dans le bon programme, il faut vérifier que le dessin correspond à la taille du support en déplacant le laser à l'aide du bouton MOVE (comme on peut voir sur la première image). Ensuite, il faut gérer les traits et les différentes découpes ou gravures que l'on souhaite pratiquer (deuxième image ci-dessous).

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/move_curseur-min.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/dif_de_trait-min.jpg)

Pour finir, vérifier que le bouton STATUS en bas à gauche de l'écran est vert. Si ce n'est pas le cas, cliquer dessus pour connaître le problème.
!A NE SURTOUT PAS OUBLIER! : allumer les extracteurs d'air, l'arriver dair comprimé et fermer la porte de la machine.
Enfin, maintenant que tout est en place, lancer la découpe à l'aide du bouton RUN. Au moindre problème ou élément suspect, appuyer sur le gros bouton rouge situé à droite de la machine, et trouver au plus vite l'extincteur le plus proche.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/GO-min.jpg)

## Changement de forme.

Après la réalisation du premier test en forme de roue, je n'étais pas convaincu par la forme de roue ou les possibilité qu'elle offrait. J'ai donc cherché à changer de forme tout en restant dans cette idée de mouvement. Après avoir approfondi mes recherches sur le Dondolo afin de compléter mon index, j'ai décidé de partir sur une forme sphérique qui conserve cette possibilité de mouvement et qui fait référence aux planètes car la chaise fut créée à l'époque des premiers voyages dans l'espace.

## La sphère.

une fois cette décision prise, il fallait que je trouve comment obtenir un globe sur base d'une feuille en papier. La première chose à laquelle j'ai pensé a été de découper la forme que l'on trouve sur certaine carte du globe qui, une fois découpée dans un papier, permet de reconstituer une sphère. Mais je n'ai pas trouvé comment définir cette forme de manière simple et précise. De plus, même si j'y parvenais, je ne voyais pas comment je pourrait refermer la forme tout en gardant une forme lisse sans utiliser de colle.

J'ai donc décidé de penser ma sphère autrement. A la place de représenter son enveloppe, j'allais essayer de représenter sa structure interne. J'ai donc défini 5 plateaux ronds de diamètres différents qui seront reliés par 6 demi-cercle.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/pieces_liste-min.jpg)

Une fois assemblés, tous ces éléments forment la sphère.

## Problèmes rencontrés.

A la première découpe, les fentes étaient trop fines et les pièces ne s'emboitaient donc pas. J'ai d'abord tenter de résoudre ce problème en jouant avec les paramètres de la machine. En augmentant la puissance, en réduisan la vitesse, ou même en éloignant le laser de la planche pour jouer avec la distance focale du rayon. Mais aucun de ces réglages n'a eu un impact suffisant pour compenser l'écart entre la largeur du trait et l'épaisseur de la plaque.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/decoupe_regl%C3%A9e-min.jpg)

J'ai donc, pour régler ce problème d'emboitement, du reprendre mon dessin sur autocad et changé chaque trait d'encoche en double trait espacé de 1mm soit l'épaisseur du plexi utilisé. Dés lors, ma sphère s'assemblait parfaitement.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/sphere_vue_cote-min.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/sphere_vue_haut-min.jpg)

## Ajout de la lumière.

une fois la forme souhaitée obtenue, le problème qui s'est présenté a moi à été d'y intégrer une lumière de quelque façon que ce soit. Ma sphère étant constituée uniquement par des éléments structurels, j'ai d'abbord pensé à placer ma "lampe" sur une source lumineuse mais je perdais alors l'effet mobile recherché au départ. Il a donc allu que je trouve un moyen d'intégrer la source lumineuse à l'objet.

Après avoir longtemps observé ma boule, l'idée m'est venue d'illuminer certains compartiments formés par la structure. Ces compartiments se forment avec un système simple de paroies et d'encoches dans les faces en demi-cercle.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/d%C3%A9tail_boites-min.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/compartiment_de_face-min.jpg)

Après l'ajout d'une guirlande qui passe d'un compartiment à l'autre à l'aide de trous dans les surfaces circulaires, on obtient le prototypes ci-dessous.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%204/lampe_finie2-min.jpg)

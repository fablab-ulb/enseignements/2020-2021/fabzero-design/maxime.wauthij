# 5. Formation Shaper

Aujourd'hui nous avons appris à nous servir de la Shaper qui est l'équivalent d'une CNC mobile. Grace à sa petite taille et sa maniabilité, elle permet de créer des pièces aux dimensions beaucoup plus variées que la CNC habituelle qui est limitée par la taille de son caisson.

Je vais vous expliquer ici les grandes lignes de son fonctionnement, mais pour plus de détails référez vous à son [guide d'utilisation](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf).

Ci-dessous, deux schémas expliquant les différents éléments de la machine.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%205/SHAPER%20%C3%A9l%C3%A9ments.png)

## Installation de la machine

Avant toutes choses voici quelques petites information de base à connaitre:

- **Format de fichier à importer**: SVG

- **Profondeur de découpe**: dépend du diamètre de la fraise

- **Profondeur maximale**: 43mm

### La fixation

Tout d'abord, il faut vérifier que l'espace de travail est bien stable et que la planche à découper est bien fixée. Vérifier également que le lieu de découpe est bien plat et qu'il n'y a aucun élément qui pourrait géner la machine.

### Délimiter la zone de travaille

Une fois l'objet bien attaché, il faut délimiter l'espace dans lequel découper. Pour cela, utiliserer le **Shaper tape** qui se présente sous forme d'un ruban de domino.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%205/Shaper%20tape.png)

A l'aide d'une caméra frontale, la machine va scanner la surface de découpe et pour qu'elle la reconnaisse convenablement, il faut au minimum 3 bandes de tape. Chaque bande de tape étant elle même composée de 4 dominos.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%205/SHAPER%20app%20tape.png)

### Changer la fraise

Avant tout, vérifier que la machine est bien débranchée. Ensuite, retirer la fraiseuse de la machine et la poser retournée à côté. Tout cela est à faire très précautionneusement car les fraises sont des éléments très fragiles et très chers.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%205/changer%20fraise.png)

Une fois la fraiseuse retirée, déviser, à l'aide d'une clef, tout en appuyant sur le bouton. Changer la fraise et ensuite revisser toujours en appuyant sur le bouton. Il n'est pas utile d'enfoncer la fraise de remplacement jusqu'au fond, mais il faut s'assurer que c'est bien serré.

## Démarrage de la machine

Une fois que toute l'installation s'est bien déroulée, que la planche est bien fixée et que la zone est délimitée convenablement avec le shaper tape, on lancer la mise en marche de la machine en suivant les étapes ci-dessous:

- Brancher l'aspirateur.

- Brancher la machine qui s'allumera par elle même.

- Lancer le scan de la zone à découper préalablement délimitée. Appuyer sur le bouton vert pour débuter le scan et sur le bouton orange pour l'arrêter.

- Insérer la clé USB contenant les fichier SVG dans la machine et cliquer sur le bouton **DESSINER** afin de visualiser la projection de votre dessin sur l'écran et de le positionner dans l'espace de découpe.

- Sélectionner **FRAISER** pour régler tous les paramètres de découpe.

- Appuyer sur **Z TOUCH** pour que la machine repère la surface du matériaux.

- Enfoncer le bouton vert pour lancer la découpe et suivre le mouvement indiqué sur l'écran. Attention à ne pas faire de mouvement trop brusque car les fraises peuvent être très vite endommagées.

# 3. Impression 3D

Après avoir modéliser la chaise a bascule, il faut maintenant l'imprimer.

## Etape 1: Importer le modèle.

Pour commencer j'ai du importer le modèle 3D en format .stl pour pouvoir le mettre dans le programme Prusaslicer.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%203/export_stl-min.png)

## Etape 2: Réglage Prusa.

Une fois le fichier .stl téléchargé, il suffit de le glisser dans le programme Prusaslicer pour l'y ouvrir.

J'ai redimensionné mon objet pour réduire le temps d'impression.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%203/prusa_import-min.png)

Il faut maintenant modifier quelques réglages d'impression de base que l'on nous a indiqué lors de la formation.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%203/reglage_1-min.png)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%203/reglages_2-min.png)

Une fois tout les régles fait, il faut appuyer sur "Découper maintenant" pour les appliquer à l'objet.

## Etape 3: Impression.

Lorsque tous les réglages ont été fait, il ne reste plus qu'à "Exporter le G-code" et le transférer sur la carte SD de l'imprimante.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%203/impression-min.png)

Replacer ensuite la carte SD avec le G-code dans l'imprimante et sélectionner le fichier ciblé pour lancer l'impression.

C'est toujours mieux de rester à côté de l'imprimante lors des 3 prmière couche pour vérifier que tout se passe bien.

## Etape 4: Objet imprimé.

Une fois que l'impression est terminée, retirer la pièce et enlever tous les supports.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%203/premier_modele_3D-min.jpg)

Dans mon cas je n'arrive pas à enlever tous les supports sans prendre le risque de casser ma pièce à cause de ces dimensions.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%203/premier_modele_d%C3%A9tail-min.jpg)

## Etape 5: Amélioration et plug-in.

Pour cette dernière étape j'ai perfectionné mon modèle 3D en ajoutant des détails présents sur la chaise réelle comme les rebors présents partout sur la chaise ou encore la courbure qui se raproche maintenant plus de l'originale. J'ai essayé pour toutes ces légères modifications, de continuer à travailler avec mêmes outils de modélisation expliqués dans le module 2. Cela m'a permit d'améliorer ma maîtrise de fusion 360.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%203/chaise_verte_profil-min.jpg)

J'ai également dans cette dernière étape, ajouté le plug-in qui est ici un espace de rangement. Ce plug-in est constitué de 4 crochets et d'un filet. Les crochets ont étés créusés dans la matière afin de conserver le côté épuré de la chaise et d'éviter l'ajout d'attaches. J'ai ensuite choisi le filet car j'ai voulu privilégier un matériaux souple pour le support qui s'adapterait aux mouvements de la chaise.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%203/chaise_verte_c%C3%B4t%C3%A9-min.jpg)
